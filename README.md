# Styles.less #

#Versions

* **1.3**
* 1.2
* 1.1
* 1.0

## What is it?

This is a styling library builded with the CSS pre-processor [LESS](http://www.lesscss.org/).

## Using Styles.less

You can use an direct link, that is easier, or you can download it and later add to your project.

## Use example:
Import the desired file to your .less file.

```
#!less
//Animations.less
@import "https://bitbucket.org/TogetherDev/styles.less/raw/c468fb0eb98f6020ab549bfaa795f6d42547a65b/less/Animations.less";

//Borders.less
@import "https://bitbucket.org/TogetherDev/styles.less/raw/c468fb0eb98f6020ab549bfaa795f6d42547a65b/less/Borders.less";

//Columns.less
@import "https://bitbucket.org/TogetherDev/styles.less/raw/c468fb0eb98f6020ab549bfaa795f6d42547a65b/less/Columns.less";

//DefaultResponsiveness.less
@import "https://bitbucket.org/TogetherDev/styles.less/raw/c468fb0eb98f6020ab549bfaa795f6d42547a65b/less/DefaultResponsiveness.less";

//FixedContent.less
@import "https://bitbucket.org/TogetherDev/styles.less/raw/c468fb0eb98f6020ab549bfaa795f6d42547a65b/less/FixedContent.less";

//GradientBackgroud.less
@import "https://bitbucket.org/TogetherDev/styles.less/raw/c468fb0eb98f6020ab549bfaa795f6d42547a65b/less/GradientBackgroud.less";

//Links.less
@import "https://bitbucket.org/TogetherDev/styles.less/raw/c468fb0eb98f6020ab549bfaa795f6d42547a65b/less/Links.less";

//Lists.less
@import "https://bitbucket.org/TogetherDev/styles.less/raw/c468fb0eb98f6020ab549bfaa795f6d42547a65b/less/Lists.less";

//NavBars.less
@import "https://bitbucket.org/TogetherDev/styles.less/raw/c468fb0eb98f6020ab549bfaa795f6d42547a65b/less/NavBars.less";

//ResponsivenessBuilder.less
@import "https://bitbucket.org/TogetherDev/styles.less/raw/c468fb0eb98f6020ab549bfaa795f6d42547a65b/less/ResponsivenessBuilder.less";

//Scrollbar.less
@import "https://bitbucket.org/TogetherDev/styles.less/raw/c468fb0eb98f6020ab549bfaa795f6d42547a65b/less/Scrollbar.less";
```

## Licensing

**Styles.less** is provided and distributed under the [Apache Software License 2.0](http://www.apache.org/licenses/LICENSE-2.0).

Refer to *LICENSE.txt* for more information.